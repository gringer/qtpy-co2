# QTPy CO₂

Code to measure and report CO₂, temperature, and humidity levels

Ingredients:

* QT Py RP2040 [microcontroller] [[#4900](https://www.adafruit.com/product/4900) $9.95]
* CO₂/temperature/humidity sensor [e.g. [#5187](https://www.adafruit.com/product/5187) $49.50]
* OLED display [e.g. [#938](https://www.adafruit.com/product/938) $19.95]
* 2 * STEMMA QT Cable [e.g. [#4210](https://www.adafruit.com/product/4210) $0.95 each]

Total hardware cost (excluding shipping, USB cable, power source): $81.30

Method:

1. Connect RP2040 to CO₂ sensor using STEMMA QT cable
1. Connect CO₂ sensor to OLED display using STEMMA QT cable
1. Connect RP2040 to computer using USB cable
1. Upload these repository files onto CIRCUITPY drive

To use:

1. Connect RP2040 to a USB power source
1. To cycle the display mode between [All -> CO₂ -> temperature -> humidity], press the BOOT button
