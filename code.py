import time
import microcontroller
import neopixel
import board
import digitalio
import adafruit_scd4x

pixel1 = neopixel.NeoPixel(board.NEOPIXEL, 1)
pixel1.fill((64,64,64))
i2c = board.STEMMA_I2C()
#i2c = busio.I2C(board.SCL1, board.SDA1) # QT Py RP2040 STEMMA connector
time.sleep(0.25)

scd4x = adafruit_scd4x.SCD4X(i2c) # CO2/temp/humidity sensor

pixel1.fill((32,32,32))
time.sleep(0.25)

print("Serial number:", [hex(i) for i in scd4x.serial_number])

scd4x.start_periodic_measurement()
print("Waiting for\n first measurement...")

button = digitalio.DigitalInOut(board.BUTTON)
btnCount = 0
lastbtnState = button.value


shownText = False
loopCount = 1000
CO2val = 0
tempVal = 0
humVal = 0

while(not scd4x.data_ready):
    CO2val = "None" if (scd4x.CO2 is None) else int(scd4x.CO2)
    print("wait... [%s]" % str(CO2val))
    for i in range(10):
        pixel1.fill((i * 10, i * 10, i*10))
        time.sleep(.1)
    for i in range(10):
        pixel1.fill(((10-i) * 10, (10-i) * 10, (10-i)*10))
        time.sleep(.1)

while(True):
    if ((loopCount > 20) or (scd4x.data_ready)):
        loopCount = 0
        if(scd4x.data_ready):
            CO2val = int(scd4x.CO2)
            tempVal = scd4x.temperature
            humVal = scd4x.relative_humidity
        if(CO2val > 1400):
            pixel1.fill((255,0,0))
        elif(CO2val > 1200):
            pixel1.fill((128,32,0))
        elif(CO2val > 1000):
            pixel1.fill((32, 32, 0))
        elif(CO2val > 800):
            pixel1.fill((16, 48, 0))
        else:
            pixel1.fill((0, 64, 0))
        text = (("CO : %d ppm\nTemperature: %4.1f  C\nHumidity: %0.1f%% RH") %
            (CO2val, tempVal, humVal))
        print(text + "\n")
        if(btnCount == 0):
            pass
        elif(btnCount == 1):
            text = ("%d" % CO2val)
        elif(btnCount == 2):
            text = (("%4.1f  C") % tempVal)
        elif(btnCount == 3):
            text = (("%4.1f%%") % humVal)
        print(text);
    if(button.value != lastbtnState):
        loopCount = 1000
        if(button.value):
            btnCount = (btnCount + 1) % 4
        lastbtnState = button.value
    loopCount += 1
    time.sleep(.05)
