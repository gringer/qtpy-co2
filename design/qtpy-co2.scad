*union(){
	import("4900 QTPy RP2040/4900 QTPY-RP2040.stl");
	translate([0,0,0]) color("lightgreen") linear_extrude(height=1.57) offset(2.54, $fn=16) offset(-2.54) square([0.7 * 25.4,0.815 * 25.4]);
}
*union(){
	import("5187 SCD-40 C02 Sensor/5187 SCD-40 C02 Sensor.stl");
	translate([0,0,0]) color("lightblue") linear_extrude(height=1.57) offset(2.54, $fn=16) offset(-2.54) square([1 * 25.4, 0.9 * 25.4]);
}

union(){
	import("938 Mono 128x64 OLED Stemma/938 Mono 128x64 OLED Stemma.stl");
	translate([0,0,0]) color("salmon") linear_extrude(height=1.57) offset(2.54, $fn=16) offset(-2.54) square([1.4 * 25.4, 1.3 * 25.4]);
	translate([3.07,11.334,-2]) color("green") linear_extrude(height=0.5)
		square([29.42, 14.7]);
}

echo(0.58 * 25.4);